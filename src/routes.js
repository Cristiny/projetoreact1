import React from 'react'
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'
import Footer from './components/footer/footer.jsx'
import Home from './components/home/home.jsx'
import Header from './components/header/header.jsx'
import Moedas from './components/moedas/moedas.jsx'
import Erro404 from './components/erro404/error404.jsx'
import Construcao from './components/construcao/construcao.jsx'

function Routes()   {
    return(
        <BrowserRouter>
        <Header/>
        <Switch> 
            <Route exact path="/">
                <Home/>
            </Route>
            <Route exact path="/cotacao">
                <Moedas/>
            </Route>
            <Route path="/">
                <Erro404/>
            <Route exact path="/construcao">
                <Construcao/>
            </Route>
            </Route>
        </Switch>
        <Footer/>
        </BrowserRouter>
    )
}

export default Routes