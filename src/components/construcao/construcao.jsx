import React from 'react'
import Imagens from '../imagens/soon.svg'
import './construcao.css'

function Construcao() {
    return(
        <>
            <h1 class="a ">Em construção</h1>
            <img class="b" src={Imagens} alt={"soon.svg"}/>
            <p class="a">Parece que essa página não foi implementada...<br/>
            Tente novamente mais tarde!</p>
            <button class="c ">VOLTAR</button>
        </>
    )
}

export default Construcao