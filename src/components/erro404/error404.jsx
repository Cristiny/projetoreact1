import React from 'react'
import Imagens6 from '../imagens/not-found.svg'
import './erro.css'

function Erro() {
    return(
        <>
            <h1 class="a"> Erro 404</h1>
            <img class="b" src={Imagens6} alt={"not-found.svg"}/>
            <p class="a">Ops! Parece que o endereço que você digitou está incorreto...</p>
            <button class="c">VOLTAR</button>
        </>
    )
}

export default Erro